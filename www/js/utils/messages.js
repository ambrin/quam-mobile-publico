define([  
], function () {
 
  //Se peude incluir internacionalizacion
   var Messages = Messages || {}; 
   
   Messages.LOGIN_CREDENTIALS_ERROR = 'El email y la contraseña no coinciden, revisalos!'; 
   Messages.LOGIN_MAIL_SENT =  'Te hemos enviado un email a tu bandeja de entrada. Comprueba la carpeta de spam';
   Messages.MAIL_DOES_NOT_EXISTS = 'El email introducido no corresponde con ningún usuario. Por favo,r comprueba que está bien escrito.';
   Messages.CONFIRM_WEB_REDIRECT = 'Será redirigido a una web externa. ¿Seguro que desea continuar?'; 
   
  return Messages;

});