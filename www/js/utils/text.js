define([  
], function () {
 
  return {

    /*
     - originalDate: string - "2014-01-29T14:36:16"
     - return 29/01/2014
    */
    formatDate: function(originalDate) {
      if(originalDate.length>10){
        originalDate= originalDate.substring(0, 10);

	    var d = new Date(originalDate),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    originalDate =  [day, month, year].join('/');


      }
      return originalDate;       
    },

    /*
     - originalDate: string - "2014-01-29T14:36:16"
     - return 29/01/2014 - 14:36
    */
    formatDateHours: function(originalDate) {

      var d = new Date(originalDate),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear(), 
          hour = '' +d.getHours(), 
          minutes = '' +d.getMinutes();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      if (hour.length < 2) hour = '0' + hour;
      if (minutes.length < 2) minutes = '0' + minutes;

      originalDate =  [day, month, year].join('/');
      originalDate += ' ' + hour + ':' + minutes;

      
      return originalDate;       
    },

    isBetween: function(dateCheck, startDate, endDate){

      if(dateCheck.length>10){
        dateCheck= dateCheck.substring(0, 10);
      }

      var d1 = startDate.split("/");
      var d2 = endDate.split("/");
      var c = dateCheck.split("/");

      var from = new Date(d1[2], d1[1]-1, d1[0]);  // -1 because months are from 0 to 11
      var to   = new Date(d2[2]+1, d2[1]-1, d2[0]);
      var check = new Date(c[2], c[1]-1, c[0]);
      return(check > from && check < to); 
    },
    isToday: function(dateCheck, dtoday){
      if(dateCheck.length>10){
        dateCheck= dateCheck.substring(0, 10);
      }
      var c = dateCheck.split("/");
      var d1= dtoday.split("/");
      var today=new Date(d1[2], d1[1]-1, d1[0]);
      var check=new Date(c[2], c[1]-1, c[0]);
      return((d1[2]==c[2]) && (d1[1]-1 == c[1]-1) && (d1[0] == c[0]));
    },

    //Devuelve lal fecha de hoy
    getToday: function(){

      var d = new Date(),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [day, month, year].join('/');     
    },




    // Calcula la fecha de un mes antes.
    //TODO si estamos en Enero, este cálculo está mal hecho
    monthAgo: function(date){
      var d = new Date();
      d.setMonth(d.getMonth() - 1);
      
      var  month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('/'); 

    },

    weekAgo: function(){
      var d = new Date();
      d.setDate(d.getDate() - 7);

      var  month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('/'); 

    },

    /*
	 - originalDate: 21/10/2013
    */
    getDaysFromToday: function(originalDate){
    	var split = originalDate.split('/');
    	var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
  		var firstDate = new Date(split[2],split[1]-1,split[0]);
  		var secondDate = new Date();

  		var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
  		console.log('fecha: ' + originalDate); 
  		console.log('dias restantes: '+ diffDays);
  		return diffDays; 
    },

    /*
      startDate = dd/mm/yyyy
      endDate = dd/mm/yyyy
    */
    getDaysBetweenTwoDates: function(startDate, endDate){
      var f1 = startDate.split('/');
      var f2 = endDate.split('/');
      var date1 = new Date([f1[1], f1[0], f1[2]].join('/'));
      var date2 = new Date([f2[1], f2[0], f2[2]].join('/'));
      if(date2.getTime() < date1.getTime()){
        return -1;
      }
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      return diffDays; 

    },

    /*
	 - originalName: COMBO_MES, COMBO_SEMANA
	 - return Mes, Semana
    */
    getComboName: function(originalName){

      var res = originalName.split(" ");
      if(res.length>1){
        originalName = res[1];
      }
  		if(originalName.indexOf('_')!=-1){
  			originalName = originalName.substring(originalName.indexOf('_')+1);
  		}
  		return this.capitalize(originalName);
    }, 

    capitalize: function(string){
    	string = string.toLowerCase();
    	return string.charAt(0).toUpperCase() + string.slice(1);
    }

  };


});