define([
	'api/mockClient',
	'api/apiClient'  
], function (MockClient, ApiClient) {
  'use strict';

  var MOCK = false; //change to 'false' for API Connection
  var BROWSER = false; //poner a true si vamos a depurar en un browser. Si está a false, lanzará cosas nqtivas de android
  //que en un browser no van a funcionar.
  //Si está a true, los scripts en index.html hay que comentarlos. 

  var Api = MOCK ?  MockClient : ApiClient;
  Api = Api.init({
    baseUrl: 'http://localhost:8000/piticli',
    //baseUrl: 'http://sbob.paradigmatecnologico.com/piticli',
    //baseUrl: 'https://pre.quam.com.ar/piticli',
  });

  return {
    refreshTime: '50000', 
    api: Api, 
    browserDebug: BROWSER, 
    recargaUrl: 'http://www.quam.com.ar/recarga/publica/', 
    registerUrl: 'http://www.quam.com.ar/cuenta/alta/', 
    facebookAppId: '758627754153516', //PRE
    twitterConsumerKey: 'zb9sfkfSJTvfgPD3GMOIA',
    twitterConsumerSecret:'CyoyTTrHrH2H9042HlS4yIU6TW090bA2VvgQgkLcrqQ', 
    googleClientId:'295309328564-vk5323q5o1ueavgtkqlm8vnakrjma1rg.apps.googleusercontent.com',
    googleClientSecret:'qOT_dtFgVunrqoS_WcJenIW5'
  };
});