define([  
], function () {
 
  return {

    showEmptyError: function(element) {
      $(element).removeClass('ok').addClass('error');
      $(element).parent().removeClass().addClass('empty-error error');
      this.disableButton(element);
    },

    showFormatError: function(element) {
      $(element).removeClass('ok').addClass('error');
      $(element).parent().removeClass().addClass('format-error error');
      this.disableButton(element);
    },

    showOk: function(element) {
      $(element).removeClass('error').addClass('ok');
      $(element).parent().removeClass('format-error empty-error error');
      this.enableButton(element); 
    },

    disableButton: function(element){
      $(element).closest('form').find('button[type=submit]').attr('disabled', 'true');
    },

    enableButton: function(element){
      $(element).closest('form').find('button[type=submit]').removeAttr('disabled');   
    }, 

    checkEmail: function(element){
      var email = element.val(); 
      var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

      if(email.length == 0){
        this.showEmptyError(element);
      } else if(!regex.test(email)){
        this.showFormatError(element);
      } else {
        this.showOk(element);
        return true; 
      }

      return false; 
    },

    checkPassword: function (element) {
      var pass = element.val(); 
      var regex = /^\s*[a-zA-Z\d]{6,20}\s*$/;
        
      if(pass.length == 0){
        this.showEmptyError(element);
      } else if(!regex.test(pass)){
        this.showFormatError(element);
      } else {
        this.showOk(element);
        return true;
      } 
      return false; 
    }

  };


});