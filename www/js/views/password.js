define([
  'backbone',
  'jquery',
  'underscore', 
  'text!templates/forgot-password.html', 
  'utils/global', 
  'utils/validations',
  'utils/messages',
  'views/notification'
], function (Backbone, $, _, template, global, validations, Messages, NotificationView) {
  'use strict';

  var ForgotPasswordView = Backbone.View.extend({

    el: '#forgot-password-page',

    events: {
      'click button[type=submit]'   : 'sendEmail', 
      'focusout input[type=email]'  : 'checkEmail', 
      'click button.cancel'         : 'cancel', 
      'click button#register'       : 'register'
    },

    render: function () {
      this.$el.html(_.template(template, {} ));
      this.notification = new NotificationView();
    }, 

    sendEmail: function(){
      console.log('click en button enviar contraseña'); 
      var _this = this;

      if(this.checkEmail()){//comprobamos de nuevo por seguridad
        var email = $('input[type=email]').val();
        global.api.recovery(email, function(error, response){
          if(error){
            _this.notification.showMailDoesNotExists(); 
          }else{
            _this.goToLogin();
          }
        }); 
        
      }
      
    }, 

    checkEmail: function(){
      return validations.checkEmail($('input[type=email]')); 
    },

    goToLogin: function(){ 
      global.router.navigate('login/reset', { trigger: true });
    },

    cancel: function(evt){
     evt.preventDefault();
      console.log('click en button cancel'); 
     global.router.navigate('login/back', { trigger: true });
    },


    register: function(){
    
      if(confirm(Messages.CONFIRM_WEB_REDIRECT)){
        window.open(global.registerUrl, '_system', 'location=yes');
      }
    }


  });

  return ForgotPasswordView;
});
