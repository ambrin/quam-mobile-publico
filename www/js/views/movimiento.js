define([
  'backbone',
  'jquery',
  'underscore', 
  'models/movimiento',
  'text!templates/movimiento.html',
], function (Backbone, $, global, PaymentModel, template) {
  'use strict';

  var Payment = Backbone.View.extend({

    model: PaymentModel,

    render: function () {

      var compiledTemplate = _.template(template, this.model.toJSON());
      this.$el.html(compiledTemplate);
      return this;
    },
  });

  return Payment;
});
