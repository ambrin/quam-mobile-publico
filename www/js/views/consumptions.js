define([
  'backbone',
  'jquery',
  'jquerymobile', 
  'underscore', 
  'text!templates/consumptions.html',
  'collections/consumos', 
  'views/movimiento',
  'utils/global', 
  'utils/text'
], function (Backbone, $, mobile, _, template, ConsumosCollection, MovimientoView, global, text) {
  'use strict';

  var ConsumosView = Backbone.View.extend({

    el: '#consumos-page',
    currentSection: 'semana',

    events: {
      'click span.btn_back'           :   'back', 
      'click nav ul li'               :   'changeFilter', 
      'click span.btn_filters'        :   'toggleFilters',
      'click li.relleno1-4'           :   'toggleFilterSelected', 
      'click div.tarifa-consumption'  :   'toggleFilterSelected',
      'click button#buscar'           :   'doSearch',
      'change input[name=date1]'      :   'checkDates', 
      'change input[name=date2]'      :   'checkDates'
    },

    initialize: function(options){
      console.log('views consuptioms.js, initialize');
      this.msisdn = options.phoneNumber;
      this.model = new ConsumosCollection(null, {msisdn: this.msisdn}); 
      this.listenTo(this.model, 'consumptions:complete', this.render);//solo cuando se llama a la API para obtener todos     
      
      this.model.filterByLastWeek();
      this.init = true; 

      
    },

    render: function () {
      
      console.log('render consumos con ' + this.model.length + ' elementos');
      console.log('datos: '+JSON.stringify(this.model));
      var compiledTemplate = _.template(template, {consumptions: this.model.toJSON(), section: this.currentSection, msisdn: this.msisdn});
      this.$el.html(compiledTemplate);

      //capa de no hay resultados: 
      this.$el.find('.empty-results').css('height', $(window).height() - 80);
      this.$el.find('#layer').css('height', this.$el.find('#results').height() + this.$el.find('#movimientos-content nav').height());

      if(this.init){ //solo lo hacemos la primera vez 
        this.init = false; 
        this.model.getData(); 
        
      }
    }, 

    changeFilter: function(evt){
      console.log('views consuptioms.js, changeFilter');
      //evitamos doble click
      if($(evt.target).attr('id')=== this.currentSection){
        return; 
      }

      this.$el.find('nav ul li').removeClass('selected'); 
      $(evt.target).addClass('selected');
      this.currentSection = $(evt.target).attr('id');
      //TODO ver cuál ha pulsado, y llamar a la collection para filtrar en base a ese
      if(this.currentSection=='semana'){
        this.model.filterByLastWeek(); 
        this.render();
      } else if (this.currentSection == 'hoy') {
        this.model.filterByToday();
        this.render();
      } else {
        this.model.filterByMonth();
      }
    },

    toggleFilters: function(){
      this.$el.find('span.btn_filters').toggleClass('selected'); 
      this.$el.find('#filters-consumptions').toggleClass('show');
      this.$el.find('#layer').toggleClass('show');  

      if(this.$el.find('#filters-consumptions').hasClass('show')){

        var el = $("#filters-consumptions"),
        curHeight = el.height(),
        autoHeight = el.css('height', 'auto').height() + 5; //temporarily change
        el.css('height', '0');

        this.$el.find('input[type=date]').datepicker();
        this.$el.find('input[name=date1]').val(this.$el.find('input[name=date1]').attr('placeholder'));
        this.$el.find('input[name=date2]').val(this.$el.find('input[name=date2]').attr('placeholder')); 

        $('#filters-consumptions').animate({
          height: autoHeight 
        }, 200, function(){
          // defining options
          var options = {
            date: new Date(),
            mode: 'date'
          };
          
        });
      }else{
        $('#filters-consumptions').animate({
          height: "0"
        }, 200);
      }
    }, 

    toggleFilterSelected: function(evt){
      $(evt.currentTarget).siblings().removeClass('selected');
      $(evt.currentTarget).toggleClass('selected');
    }, 

    doSearch: function(){
      this.toggleFilters();
      $('#movimientos-content nav').hide();
      var usage = this.$el.find('li.relleno1-4.selected div').data('value');
      var rate = this.$el.find('div.containerTarifa div.selected').data('value');
      var cost = this.$el.find('div.containerCoste div.selected').data('value');

      var start = this.$el.find('input[name=date1]').val();
      var end = this.$el.find('input[name=date2]').val(); 
      if(start==$('input[name=date1]').attr('placeholder')){
        start = null;
      }
      if(end==$('input[name=date2]').attr('placeholder')){
        end = null;
      }
      this.model.initConsumption(start, end, usage, rate, cost); 
    }, 

    checkDates: function(){
      
      var start = this.$el.find('input[name=date1]').val();
      var end = this.$el.find('input[name=date2]').val();

      if(start!=$('input[name=date1]').attr('placeholder') && 
        end!=$('input[name=date2]').attr('placeholder')){

        var diffDays = text.getDaysBetweenTwoDates(start, end); 
        if(diffDays==-1){
          alert('La fecha de inicio debe ser anterior a la fecha de fin');
          this.$el.find('input[name=date1]').val(this.$el.find('input[name=date1]').attr('placeholder'));
          this.$el.find('input[name=date2]').val(this.$el.find('input[name=date2]').attr('placeholder')); 
        } else if (diffDays>30){
          alert('Solo se pueden consultar consumos en el periodo de 1 mes. Por favor, revise sus datos.');
          this.$el.find('input[name=date1]').val(this.$el.find('input[name=date1]').attr('placeholder'));
          this.$el.find('input[name=date2]').val(this.$el.find('input[name=date2]').attr('placeholder')); 
        }

        return (diffDays>0 && diffDays <31);
        
      }
      
    },

    back: function(){
      global.router.navigate('consumosresumen/'+this.msisdn, { trigger: true });
    }, 

  });
  return ConsumosView;
});
