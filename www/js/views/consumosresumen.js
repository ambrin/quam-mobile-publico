define([
  'backbone',
  'jquery',
  'underscore', 
  'text!templates/consumptionssumary.html',
  'models/consumptionssummary',
  'views/consumptions',
  'utils/global'
], function (Backbone, $, _, template, ConsumptionsSummary, ConsumptionsView, global) {
  'use strict';

  var summaryView = Backbone.View.extend({

    el: '#consumossummary-page',
    currentSection: 'semana',

    events: {
      'click span.btn_back'           :   'back', 
      'click nav ul li'               :   'changeFilter',
      'click section div'             :   'goToConsumption'
      },

    initialize: function(){
      console.log('views consumosresumen.js, initialize');
      this.msisdn = this.options.phoneNumber;
      this.model = new ConsumptionsSummary(null, {msisdn: this.msisdn, api: this.options.api}); 
      this.listenTo(this.model, 'consumptionssummary:complete', this.render);//solo cuando se llama a la API para obtener todos     
      this.init = true; 
    },

    render: function () {
      console.log('mostramos consumosresumen'); 
      var compiledTemplate =_.template(template, this.model.toJSON());
      this.$el.html(compiledTemplate);   

      if(this.init && this.options.api){ //solo la primera vez que pasemos por el render
        this.model.getData();
        this.init = false; 
      }  
    },
    goToConsumption: function (){
      //alert("Section actual: "+this.currentSection+" número de cuenta: "+this.msisdn); 
      global.router.navigate('consumos/' + this.msisdn, { trigger: true });
    },
    changeFilter: function(evt){
      console.log('views consuptioms.js, changeFilter');
      //evitamos doble click
      if($(evt.target).attr('id')=== this.currentSection){
        return; 
      }
      this.$el.find('nav ul li').removeClass('selected'); 
      $(evt.target).addClass('selected');
      this.currentSection = $(evt.target).attr('id');
      this.$el.find('#consumosresumen-content section').css('display', 'none');
      if(this.currentSection=='semana'){   
         this.$el.find('#results-lastweek').css('display', 'block'); 
      } else if (this.currentSection == 'hoy') {
       this.$el.find('#results-today').css('display', 'block');
      } else if (this.currentSection == 'mes') {
        this.$el.find('#results-lastmonth').css('display', 'block');
      }
    },

    back: function(){
      global.router.navigate('home', { trigger: true });
    }

  });
  return summaryView;
});