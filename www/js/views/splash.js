define([
  'backbone',
  'jquery',
  'underscore', 
  'text!templates/splash.html'
], function (Backbone, $, _, template) {
  'use strict';

  var SplashView = Backbone.View.extend({

    el: '#splash-page',

    render: function () {
      this.$el.html(_.template(template, {} ));
      this.$el.find('#splash-page').height();
      this.$el.find('#splash_logo').css('height', this.$el.height()); 
    }
   
  });

  return SplashView;
});
