define([
  'backbone',
  'jquery',
  'underscore', 
  'text!templates/login.html', 
  'utils/global', 
  'utils/validations', 
  'utils/messages',
  'views/notification', 
  'vendor/twitter/codebird'
], function (Backbone, $, _, template, global, validations, Messages, NotificationView, codebird) {
  'use strict';

  var LoginView = Backbone.View.extend({

    el: '#login-page',

    events: {
      'submit #login-form'              : 'doLogin', 
      'focusout input[type=email]'      : 'checkEmail', 
      'focusout input[type=password]'   : 'checkPassword', 
      'focus input'                     : 'removeValidation', 
      'click button#forget-password'    : 'goToForgotPassword',
      'click button#register'           : 'recharge', 
      'click .socials > .facebook'      : 'loginFacebook', 
      'click .socials > .twitter'       : 'loginTwitter', 
      'click .socials > .google'        : 'loginGoogle'
    },

    initialize: function(){


      console.log('init loginView');

      
      if(global.browserDebug){
        // PARA PROBAR EN WEB

        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "http://connect.facebook.net/en_ES/all.js";
          fjs.parentNode.insertBefore(js, fjs);
          console.log('*************script cargado!');
        }(document, 'script', 'facebook-jssdk'));

        window.fbAsyncInit = function() {
          console.log('async init ejecutado!!');
          FB.init({
            appId  : global.facebookAppId,
            status : true, // check login status
            cookie : true, // enable cookies to allow the server to access the session
            xfbml  : true  // parse XFBML
          });
        };
 
      } else{
        /* PARA ANDROID */   
        FB.init({
            appId: global.facebookAppId,
            nativeInterface: CDV.FB,
            useCachedDialogs: false
        });
    
        // consulta de prueba
        FB.getLoginStatus(function(response){
          console.log('FB.getLoginStatus: ' + response.status);
        });
 
      }

    },

    render: function () {
      this.$el.html(_.template(template, {} ));      
      this.notification = new NotificationView();
      if(this.options.notification){
        this.notification.showRecoveryMailSent(); 
      }
    }, 

    doLogin: function(evt){
      evt.preventDefault();

      var _this = this; 
      //chequeamos de nuevo por seguridad
      if(this.checkEmail() && this.checkPassword()){
        var email = $('input[type=email]').val();
        var pass = $('input[type=password]').val();

        global.api.login(email, pass, function(error, response){
          if(error){
            _this.notification.showCredentialsError(); 
          }else{
            console.log('token obtenido: ' + response.token);
            global.auth.set({email: email, password: pass,  loggedIn: true, authToken: response.token, type: "normal" });
            $.mobile.changePage( "#home-page" , { transition: "slide",reverse: false} ); 
            global.router.navigate('home', { trigger: true });
          }
        }); 
      } 
      
    }, 

    checkEmail: function(){
      return validations.checkEmail($('input[type=email]')); 
    },

    checkPassword: function (evt) {
      return validations.checkPassword($('input[type=password]')); 
    }, 

    goToForgotPassword: function(){     
      global.router.navigate('password', { trigger: true });
    }, 

    recharge: function (){

      if(confirm(Messages.CONFIRM_WEB_REDIRECT)){
        window.open(global.registerUrl, '_system', 'location=yes');
      }
      
    }, 

    loginFacebook: function(evt){
      var _this = this; 
      $(evt.currentTarget).addClass('selected');
      FB.login(
        function(response) {
          console.log('FB.login with permissions callback');
          $(evt.currentTarget).removeClass('selected');

          var fbToken = response.authResponse.accessToken; 

          //Android: response.authResponse.userId (por el plugin)
          //Web: response.authResponse.userID
          var fbUserId = (global.BROWSER_DEBUG) ? response.authResponse.userID : response.authResponse.userId; 
 
          console.log('token obtenido facebook: ' + fbToken);
          console.log('userID obtenido de facebook: '+ fbUserId);
          global.api.loginFacebook(fbUserId, fbToken, function(error, response){
            if(error){
              _this.notification.showCredentialsError(); 
            }else{
              console.log('token obtenido simfonics: ' + response.token);
              global.auth.set({email: fbUserId, password: fbToken,  loggedIn: true, authToken: response.token, type: "facebook"});
              $.mobile.changePage( "#home-page" , { transition: "slide",reverse: false} ); 
              global.router.navigate('home', { trigger: true });
            }
          });
        },
        { scope: 'email' }
      );
    }, 

    loginTwitter: function(evt){
      var _this = this; 
      $(evt.currentTarget).addClass('selected');
      
      var cb = new Codebird;
      cb.setConsumerKey(global.twitterConsumerKey, global.twitterConsumerSecret);


      cb.__call(
        "oauth_requestToken",
        {oauth_callback: "http://localhost/"},
        function (reply) {
           console.log('callback de request token. '); 
           console.log(reply.oauth_token);
            // stores it
            cb.setToken(reply.oauth_token, reply.oauth_token_secret);

            // gets the authorize screen URL
            cb.__call(
                "oauth_authorize",
                {},
                function (auth_url) {
                   
                    var ref = window.open(auth_url, '_blank', 'location=no'); // redirection.
                    // check if the location the phonegap changes to matches our callback url or not
                    ref.addEventListener("loadstart", function(iABObject) {
                      if(iABObject.url.match(/localhost/)) {
                        ref.close();
                        console.log('peticion authorize hecha, hay que parsear...');
                        

                        var currentUrl = iABObject.url;
                        var query = currentUrl.match(/oauth_verifier(.+)/);
                        for (var i = 0; i < query.length; i++) {
                          var parameter = query[i].split("=");
                          if (parameter.length === 1) {
                            parameter[1] = "";
                          }
                        }
                        cb.__call(
                          "oauth_accessToken", {oauth_verifier: parameter[1]},
                          function (reply) {
                            cb.setToken(reply.oauth_token, reply.oauth_token_secret);
                            console.log('access_token obtenido: '+reply.oauth_token);

                            var oauth_token = reply.oauth_token; 
                            var oauth_token_secret = reply.oauth_token_secret;

                            $('.socials > .twitter').removeClass('selected');

                            //no se por qué esta haciendo la peticion dos veces. la segunda vez el token es undefined
                            if(reply.oauth_token!=undefined){
                              global.api.loginTwitter(oauth_token,oauth_token_secret, function(error, response){
                                if(error){
                                  _this.notification.showCredentialsError(); 
                                }else{
                                  console.log('token obtenido simfonics: ' + response.token);
                                  global.auth.set({email: oauth_token, password: oauth_token_secret,  loggedIn: true, authToken: response.token, type: "twitter"});
                                  $.mobile.changePage( "#home-page" , { transition: "slide",reverse: false} ); 
                                  global.router.navigate('home', { trigger: true });
                                }
                              });
                            }           
                          }
                        );


                      }
                    }); 

                }
            );
        }
      );

    }, 



    loginGoogle: function(evt){
      var _this = this; 
      $(evt.currentTarget).addClass('selected');
      console.log('login con google');

       //Inicializacion login social con Google +
      var googleapi = {
        authorize: function(options) {
            var deferred = $.Deferred();
            deferred.reject({ error: 'Not Implemented' });
            var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
                client_id: options.client_id,
                redirect_uri: options.redirect_uri,
                response_type: 'code',
                scope: options.scope
            });

            //1- abre una ventana por encima para pedir el login en Google+ y solicitar permisos
            var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');


            $(authWindow).on('loadstart', function(e) {
              var url = e.originalEvent.url;
              var code = /\?code=(.+)$/.exec(url);
              var error = /\?error=(.+)$/.exec(url);

              //2- obtiene el código para poder consultar el token
              if (code || error) {
                authWindow.close();
                console.log("code obtenido!"); 
                console.log(code);

                if (code) {
                    $.post('https://accounts.google.com/o/oauth2/token', {
                      code: code[1],
                      client_id: options.client_id,
                      redirect_uri: options.redirect_uri,
                      grant_type: 'authorization_code'
                    }).done(function(data) {
                      
                      console.log('Token Google+: ' + data.access_token);
                      
                      var googleToken = data.access_token; 
                      var googleId = ''; 
                      //3- hay que obtener el userId
                      $.ajax({
                          url: 'https://www.googleapis.com/plus/v1/people/me',
                          type: 'GET',
                          beforeSend: function (request){              
                            request.setRequestHeader('Authorization','Bearer '+googleToken);                              
                          },
                          success: function(response) {
                            console.log('google api ME ok!!!');
                            console.log('userID en google: ' + response.id);
                            googleId = response.id; 
                            $('.socials > .google').removeClass('selected');

                            //4) llamamos a sinfonics para pedir el token para la app
                            global.api.loginGoogle(googleId, googleToken, function(error, response){
                              if(error){
                                _this.notification.showCredentialsError(); 
                              }else{
                                console.log('token obtenido simfonics: ' + response.token);
                                global.auth.set({email: googleId, password: googleToken,  loggedIn: true, authToken: response.token, type: "google"});
                                $.mobile.changePage( "#home-page" , { transition: "slide",reverse: false} ); 
                                global.router.navigate('home', { trigger: true });
                              }
                            });

                          },
                          error: function(xhr, ajaxOptions, thrownError) {
                             console.log('google api ME FAIL');
                             console.log(thrownError);
                             console.log(xhr.responseText);
                             _this.failLoginGoogle();
                          }
                       })


                    }).fail(function(response) {
                      console.log('fail'); 
                      deferred.reject(response.responseJSON);
                      _this.failLoginGoogle();
                    });
                  } else if (error) {
                    console.log('error'); 
                    deferred.reject({
                      error: error[1]
                    });
                    _this.failLoginGoogle();
                  }

              }

            });


        }
      };


      googleapi.authorize({
        client_id: global.googleClientId,
        client_secret: global.googleClientSecret,
        redirect_uri: 'http://localhost', 
        scope: 'email'
      });


    }, 

    failLoginGoogle: function(){
      _this.notification.showLoginGoogleError();
      $('.socials > .google').removeClass('selected');
    }

  });

  return LoginView;
});
