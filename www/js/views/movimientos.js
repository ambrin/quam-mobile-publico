define([
  'backbone',
  'jquery',
  'underscore', 
  'text!templates/movimientos.html',
  'collections/movimientos', 
  'views/movimiento',
  'utils/global'
], function (Backbone, $, _, template, PaymentsCollection, MovimientoView, global) {
  'use strict';

  var MovimientosView = Backbone.View.extend({

    el: '#movimientos-page',
    currentSection: 'todos',

    events: {
      'click span.btn_back'           :   'back', 
      'click nav ul li'               :   'changeFilter',
      'click nav ul li#recargas'      :   'getRecargas',
      'click nav ul li#gastos'        :   'getCompras', 
      'click #results_todos .more'    :   'getMore',
      'click #results_recargas .more' :   'getMoreRecargas',
      'click #results_gastos .more'   :   'getMoreCompras'
    },

    initialize: function(){
      this.model = new PaymentsCollection();
      this.listenTo(this.model, 'payments:complete', this.render);//solo cuando se llama a la API para obtener todos
      this.listenTo(this.model, 'recharges:paint', this._renderRechargePayment);
      this.listenTo(this.model, 'purchases:paint', this._renderPurchasePayment);
      this.listenTo(this.model, 'recharges:reset', this._resetRecharges);
      this.listenTo(this.model, 'purchases:reset', this._resetPurchases);
      this.listenTo(this.model, 'recharges:complete', this._emptyRecharges);
      this.listenTo(this.model, 'purchases:complete', this._emptyPurchases);
      this.listenTo(this.model, 'show:more', this._showMoreButton);

      this.init = true;
    },

    render: function () {
      var compiledTemplate = _.template(template, {payments: this.model.toJSON()});
      this.$el.html(compiledTemplate);

      //capa de no hay resultados: 
      this.$el.find('.empty-results').css('height', $(window).height() - 80);

      if(this.init){ //solo lo hacemos la primera vez 
        this.model.getData(); 
        this.init = false; 
      }
    }, 

    _renderRechargePayment: function(payment){
      this._renderPayment(payment, 'results_recargas'); 
    },
    _renderPurchasePayment: function(payment){
      this._renderPayment(payment, 'results_gastos'); 
    },

    _renderPayment: function (payment, section) {
      console.log('rendering....');
      var movView = new MovimientoView({model: payment});
      this.$el.find('#'+section+' ul').append(movView.render().el);
    },

    _showMoreButton: function(){
      this.$el.find('#results_'+this.currentSection+' .pagination').css('display', 'block');
    }, 

    _hideMoreButton:function(){
      this.$el.find('#results_'+this.currentSection+' .pagination').css('display', 'none');
    },

    _resetRecharges: function(payment){
      this.$el.find('#results_recargas ul').html('');
      this.$el.find('#results_recargas .empty-results').hide();
    },
    _resetPurchases: function(payment){
      this.$el.find('#results_gastos ul').html('');
      this.$el.find('#results_gastos .empty-results').hide();
    },

    _emptyRecharges: function(size){
      if(size == 0) this.$el.find('#results_recargas .empty-results').show();
    },
    _emptyPurchases: function(size){
      if(size == 0) this.$el.find('#results_gastos .empty-results').show();
    },

    back: function(){
      global.router.navigate('home', { trigger: true });
    }, 

    changeFilter: function(evt){
      this.$el.find('nav ul li').removeClass('selected'); 
      $(evt.target).addClass('selected');

      this.$el.find('#movimientos-content section').css('display', 'none');
      this.$el.find('#results_'+$(evt.target).attr('id')).css('display', 'block'); 
      this.currentSection = $(evt.target).attr('id'); 
    }, 

    getMore: function(){
      this._hideMoreButton(); 
      this.model.getData(); 
    }, 
    // Solo consultará la API la primera vez. El resto, mostrará lo que ya había
    getRecargas: function(){
      this.model.filterRechargedPayments(); 
    },

    getMoreRecargas: function(){
      this._hideMoreButton(); 
      this.model.getRechargesFromApi(); 
    },

    /*
      Solo consultará la API la primera vez. El resto, mostrará lo que ya había
    */
    getCompras: function(){
      this.model.filterPurchasedPayments();   
    }, 

    getMoreCompras: function(){
      this._hideMoreButton(); 
      this.model.getPurchasesFromApi();   
    }
    

  });

  return MovimientosView;
});
