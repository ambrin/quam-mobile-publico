define([
  'backbone', 
  'utils/messages'
], function (Backbone, Message) {
  'use strict';

  var Notification = Backbone.View.extend({

    message: '', 

    el: '#notification',

    events : {      
      'click': 'hide', 
    },

    initialize: function () {
      this.message = Message.LOGIN_CREDENTIALS_ERROR; 
    },

    changeMessage: function(){
      console.log('notificacion mostrada: ' + this.message);
      $(this.$el).find('div#message').html(this.message); 
    },

    render: function(){
      var _this = this; 
      this.changeMessage(); 
      $(this.$el).css('display', 'block');
      $(this.$el).animate({
        top: "0"
      }, 500, function(){
        setTimeout(_.bind(_this.hide, _this), 5000); 
      });
    }, 

    hide: function(){
      $(this.$el).animate({
        top: "-125px"
      }, 800);
    }, 

    showCredentialsError: function(){
       this.message = Message.LOGIN_CREDENTIALS_ERROR; 
       this.render(); 
    },

    showRecoveryMailSent: function(){
      this.message = Message.LOGIN_MAIL_SENT; 
      this.render(); 
    },

    showMailDoesNotExists: function(){
      this.message = Message.MAIL_DOES_NOT_EXISTS; 
      this.render(); 
    }

  });

  return Notification;
});
