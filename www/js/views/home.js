define([
  'backbone',
  'jquery',
  'underscore', 
  'text!templates/home.html', 
  'utils/global', 
  'utils/messages',
  'collections/customerLines', 
  'views/combo',
  'views/consumosresumen'
], function (Backbone, $, _, template, global, Messages, CustomerLines, ComboView, summaryView) {
  'use strict';

  var HomeView = Backbone.View.extend({

    el: '#home-page',

    events: {
      'click button#next':        'nextPage', 
      'mouseover #menu li':       'hover', 
      'click #user-lines ul li':  'changeLine', 
      'click #combo-detail':      'comboView',
      'click #movimientos':       'goToMovimientos', 
      'click #consumos':          'goToConsumosResumen',
      'click span.btn_back':      'render', 
      'click li#recargar':        'goToBrowser'
    },

    initialize: function(){
      this.model = new CustomerLines();       
     
      this.listenTo(this.model, 'lines:complete', this.render);//solo cuando se llama a la API
      this.listenTo(this.model, 'lines:reset', this.render);//cuando se selecciona otro
      this.listenTo(this.model, 'combo:populate', this.populateComboInfo);//cuando se selecciona otro
      this.init = true; 
    },

    render: function () {
      var compiledTemplate = _.template(template, {lines: this.model.toJSON(), selectedLine: this.model.getSelectedLine().toJSON()});
      this.$el.html(compiledTemplate);
      this.hideBack(); 
      if(this.$el.find('#user-lines ul li.selected').length>0){
        this.$el.find('#user-lines ul').scrollLeft(this.$el.find('#user-lines ul li.selected').position().left - 35); 
      }
      this.$el.find('section#menu ul li').css('height', ($('body').height() - $('section#user-lines').height())/5 );

      if(this.init){
        this.init= false; 
        this.model.getData(); //lo hacemos despues del render, xq sino no pinta el spinner de loading...
      }
    }, 

    populateComboInfo: function(combo){
      if(combo.get('empty')){
        this.$el.find('li#combo-detail .left-content').hide(); 
        this.$el.find('li#combo-detail .content').hide();
        this.$el.find('li#combo-detail #empty').show();
      } else {
        this.$el.find('li#combo-detail .left-content').show(); 
        this.$el.find('li#combo-detail .content').show();
        this.$el.find('li#combo-detail #empty').hide();

        this.$el.find('#combo-detail #name').html(combo.get('name')); 
        this.$el.find('#combo-detail #days-left').html(combo.get('days_left')); 
        this.$el.find('#combo-detail #expiration').html(combo.get('expiration_date'));
      }
    },

    nextPage: function(){
      global.router.navigate('login', { trigger: true });
    }, 

    hover: function (evt){
      $('#' + evt.currentTarget.id + ' span.arrow').css('right', '16px');
      $('#' + evt.currentTarget.id + ' span.arrow').animate({'right': "10px"}, 150);
    }, 

    changeLine: function(evt){
      this.model.changeLine($(evt.currentTarget).html());
    }, 

    comboView: function(){
      /* TODO hay que pasarle directamente el model. selectedLine().combo */
      new ComboView({combo : this.model.getSelectedLine().get('combo')}).render(); 
      this.showBack(); 
    }, 

    hideBack: function(){
      this.$el.find('span.btn_back').hide(); 
    }, 

    showBack: function(){
      this.$el.find('span.btn_back').show(); 
    }, 

    goToMovimientos: function(){
      global.router.navigate('movimientos', { trigger: true });
    },

    goToConsumos: function(){
      global.router.navigate('consumos/' + this.model.getSelectedLine().get('id'), { trigger: true });
    }, 
    goToConsumosResumen: function(){
      console.log("numero de linea: "+this.model.getSelectedLine().get('id'));
      global.router.navigate('consumosresumen/'+this.model.getSelectedLine().get('id'), { trigger: true });
      
      /*new summaryView({acumulados : this.model.getSelectedLine().get('acumulados')}).render(); 
      this.showBack();*/ 
    },

    goToBrowser: function(){
      
      if(confirm(Messages.CONFIRM_WEB_REDIRECT)){
        window.open(global.recargaUrl, '_system', 'location=yes');
      }
    }

  });

  return HomeView;
});
