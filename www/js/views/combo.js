define([
  'backbone',
  'jquery',
  'underscore', 
  'text!templates/combo.html', 
  'utils/global', 
  'models/comboDetail'
], function (Backbone, $, _, template, global, ComboDetail) {
  'use strict';

  var ComboView = Backbone.View.extend({

    el: '#menu',

    events: {
      'click nav ul li' :  'changeTab', 
      'click .notification-box button': 'recharge'
    },

    initialize: function(){
      this.model = this.options.combo;
    },

    render: function () {
      console.log('rendering combo detail...');
      //inicialmente pintamos con los datos del localstorage
      var compiledTemplate = _.template(template, this.model.toJSON());
      this.$el.html(compiledTemplate);
    }, 

    changeTab: function(evt){
      this.$el.find('nav ul li').removeClass('selected'); 
      $(evt.target).closest('li').addClass('selected');

      this.$el.find('div.detail').css('display', 'none');
      this.$el.find('div#'+$(evt.target).closest('li').attr('id')+'-detail').css('display', 'block'); 
    }, 

    recharge: function (){
      var ref = window.open(global.recargaUrl, '_blank', 'location=yes');
    }


  });

  return ComboView;
});
