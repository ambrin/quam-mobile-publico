define([], function () {
  'use strict';

  return {
    store: function (email, password, authToken, callback) {
      localStorage.setItem('credentials', JSON.stringify({
        email: email,
        password: password, 
        token: authToken
      }), callback);
    },

    load: function (callback) {
      
      var email = null,
      password = null, 
      token = null; 

      if(localStorage.getItem('credentials')!=null){
        var credentials = JSON.parse(localStorage.getItem('credentials'));
        if (credentials) {
          email = credentials.email;
          password = credentials.password;
          token = credentials.token;
        }
      }


      if (callback) {
        callback(email, password, token);
      }
 
    },

    clear: function (callback) {
      localStorage.removeItem('credentials', callback);
    }, 


    //lineas de un usuario
    storeCustomerLines: function(json, callback){
      localStorage.setItem('customerLines', JSON.stringify(json));
      if (callback) {
        callback();
      }
    }, 

    loadCustomerLines: function(callback){
      var lines = null; 
      //Esta comprobacion es para las versiones 2.3.3 de android. Si está a null, 
      // el JSON.parse falla y cierra la app
      if(localStorage.getItem('customerLines')!=null){
        lines = JSON.parse(localStorage.getItem('customerLines'));
      }
      if (callback) {
        callback(lines);
      }
    }, 

    // Detalle del combo de cada linea de usuario
    storeCombo: function(json, callback){
      localStorage.setItem('combo_'+json.msisdn, JSON.stringify(json));
      if (callback) {
        callback();
      }
    }, 

    loadCombo: function(msisdn, callback){
      var combo = null; 
      if(localStorage.getItem('combo_'+msisdn)!=null){
        combo = JSON.parse(localStorage.getItem('combo_'+msisdn));
      }
      if (callback) {
        callback(combo);
      }
    }, 

    // listado de movimientos de saldo
    storePayments: function(json, callback){
      localStorage.setItem('customerPayments', JSON.stringify(json));
      if (callback) {
        callback();
      }
    }, 

    loadPayments: function(callback){
      var payments = null; 
      if(localStorage.getItem('customerPayments')!=null){
        payments = JSON.parse(localStorage.getItem('customerPayments'));
      }
      if (callback) {
        callback(payments);
      }
    }, 

    // listado de movimientos de saldo: RECARGAS
    storeRecharges: function(json, callback){
      localStorage.setItem('customerRecharges', JSON.stringify(json));
      if (callback) {
        callback();
      }
    }, 

    loadRecharges: function(callback){
      var recharges = null; 
      if(localStorage.getItem('customerRecharges')!=null){
        recharges = JSON.parse(localStorage.getItem('customerRecharges'));
      }

      if (callback) {
        callback(recharges);
      }
    },    

    // listado de movimientos de saldo: RECARGAS
    storePurchases: function(json, callback){
      localStorage.setItem('customerPurchases', JSON.stringify(json));
      if (callback) {
        callback();
      }
    }, 

    loadPurchases: function(callback){
      var lines = null; 
      if(localStorage.getItem('customerPurchases')!=null){
        lines = JSON.parse(localStorage.getItem('customerPurchases'));
      }
      if (callback) {
        callback(lines);
      }
    },

    // listado de movimientos de saldo
    storeConsumptions: function(json, callback){
      localStorage.setItem('customerConsumptions', JSON.stringify(json));
      //console.log(JSON.stringify(json)+', datos sin stringify: '+json);
      if (callback) {
        callback();
      }
    }, 

    loadConsumptions: function(callback){
      var lines = null; 

      if(localStorage.getItem('customerConsumptions')!=null){
        lines = JSON.parse(localStorage.getItem('customerConsumptions'));
      }
      
      if (callback) {
        callback(lines);
      }
    }, 

     // Detalle del combo de cada linea de usuario
    storeAcumulados: function(json, callback){
      localStorage.setItem('acumulados_'+json.msisdn, JSON.stringify(json));
      console.log("datos que se han guardado: "+JSON.stringify(json));
      if (callback) {
        callback();
      }
    }, 

    loadAcumulados: function(msisdn, callback){
      var acumulados  = null; 
      if(localStorage.getItem('acumulados_'+msisdn)!=null){
        acumulados = JSON.parse(localStorage.getItem('acumulados_'+msisdn));
      }
      if (callback) {
        callback(acumulados);
      }
    }, 

  };
});
