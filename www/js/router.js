define([
  'backbone',
  'underscore',
  'jquery',
  'views/splash',
  'views/consumosresumen',
  'views/home', 
  'views/login',
  'views/password',
  'views/combo',
  'views/movimientos',
  'views/consumptions',
  'utils/global'
], function (Backbone, _, $, SplashView, SummaryView, HomeView, LoginView, PasswordView, ComboDetailView, MovimientosView, ConsumptionsView, global) {
  'use strict';
  //ConsumptionsSummaryView,


  var Router = Backbone.Router.extend({

    initialize: function () {
      console.log('Dentro de router.js, initialize');
      Backbone.history.start();
      this.history = [];
      var _this = this;
      this.listenTo(this, 'route', function (name, args) {
        _this.history.push({
          name : name,
          args : args,
          fragment : Backbone.history.fragment
        });
      });
    },

    routes: {
      '':                         'splash',
      's':                        'splash',
      'login':                    'login',
      'login/back':               'loginReverse',  
      'login/reset':              'loginResetPassword',
      'password':                 'forgotPassword', 
      'home':                     'home', 
      'movimientos':              'movimientos',
      'consumos/:id':             'consumos',
      'consumosresumen/:id':      'consumosresumen', 
      'back':                     'back',            
     },

    splash: function(){
      $.mobile.changePage( "#splash-page" ,{reverse: false} ); 
      this.show(new SplashView());   
    },

    login: function () {
      $.mobile.changePage( "#login-page" ,{reverse: false} ); 
      this.show(new LoginView({notification: false}));   
    },

    loginReverse: function () {
      $.mobile.changePage( "#login-page" , { transition: "slide",reverse: true} ); 
      this.show(new LoginView({notification: false}));   
    },

    loginResetPassword: function(){
      $.mobile.changePage( "#login-page" , { transition: "slide",reverse: true} ); 
      this.show(new LoginView({notification: true}));   
    },

    forgotPassword: function(){
      $.mobile.changePage( "#forgot-password-page" , { transition: "slide",reverse: false} );
      this.show(new PasswordView());   
    }, 
   
    home: function (){     

      var reverseTransition = this.previousPage() == 'movimientos' 
          || this.previousPage().indexOf('consumos')!=-1 ? true : false;
  
      $.mobile.changePage( "#home-page" ,{reverse: reverseTransition} ); 
      this.show(new HomeView());
      console.log('router: home mostrada'); 
    }, 

    combo: function(){
      $.mobile.changePage( "#combo-page" , { transition: "slide",reverse: false} );
      this.show(new ComboDetailView());   
    },

    movimientos: function(){
      $.mobile.changePage( "#movimientos-page" , { transition: "slide",reverse: false} );
      this.show(new MovimientosView());   
    },

    consumosresumen: function(id){
      var reverseTransition = this.previousPage().indexOf('consumos/')!=-1 ? true : false;

      $.mobile.changePage( "#consumossummary-page" , { transition: "slide",reverse: reverseTransition} );
      this.show(new SummaryView({phoneNumber: id, api: !reverseTransition}));
    },

    consumos: function(id){
      $.mobile.changePage( "#consumos-page" , { transition: "slide",reverse: false} );
      this.show(new ConsumptionsView({phoneNumber: id}));   
    }, 
   
    back: function (){     
      $.mobile.changePage( "#home-page" ,{ transition: "slide", reverse: true} ); 
      this.show(new HomeView()); 
    }, 
    show: function (view) {
      if (this.currentView) {
        // We use these methods instead of .remove() because remove()
        // deletes the View main element
        this.currentView.stopListening();
        this.currentView.undelegateEvents();
        this.currentView.$el.empty();

        // If defined call the close method of the view
        if (typeof this.currentView.close !== 'undefined') {
          this.currentView.close();
        }

        // Views can define a clear() method which should clean them for
        // avoiding memory leaks
        if (typeof this.currentView.clear !== 'undefined') {
          this.currentView.clear();
        }
      }

      this.currentView = view;
      this.currentView.render();
    }, 

    previousPage: function(){
      if(this.history && this.history.length >0){
       return this.history[this.history.length-1].fragment; 
      } else {
        return '';
      }
    }
  });

  return Router;
});
