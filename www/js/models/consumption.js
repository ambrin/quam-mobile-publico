define([
  'backbone'
], function (Backbone) {
  'use strict';

  var ConsumtionModel = Backbone.Model.extend({
    defaults: function () {
      return {
       /* type: '',
        usage: '',
        date: '',
        name: '', 
        value:'0'
       */
        date: '',
        destination_phone: '',
        duration: '',
        size: '',
        cost: '',
        usage: ''

      };
    }
  });

  return ConsumtionModel;
});
