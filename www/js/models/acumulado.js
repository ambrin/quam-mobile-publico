define([
  'backbone',
], function (Backbone) {
  'use strict';

  var periodos = ["today", "last_week","last_month"]; 

  var ConsumoAcumuladoPeriodo = Backbone.Model.extend({
    defaults: function () {
      return {
            
        periodo: periodos[0],
        llamadasentrantes: 0,
        llamadassalientes: 0,
        internet:0,
        sms:0 
      };
    } 
    
  });

  return ConsumoAcumuladoPeriodo;
});