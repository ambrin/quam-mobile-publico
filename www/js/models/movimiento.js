define([
  'backbone',
  'utils/global', 
  'storage/auth', 
  'models/comboDetail'
], function (Backbone, global, storage, ComboDetail) {
  'use strict';

  var types = ["recharge", "purchase"]; 

  var MovimientoModel = Backbone.Model.extend({
    defaults: function () {
      return {
        type: types[0],
        subtype: '',
        date: '',
        name: '', 
        value:'0'
      };
    }
  });

  return MovimientoModel;
});
