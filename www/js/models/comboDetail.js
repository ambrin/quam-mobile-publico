define([
  'backbone',
  'utils/global', 
  'models/tipoConsumo',  
  'storage/auth', 
  'utils/text'
], function (Backbone, global, TipoConsumo, storage, text) {
  'use strict';

  var ComboDetail = Backbone.Model.extend({
    defaults: function () {
      return {
        msisdn: null, // línea a la que pertenece el combo
        name: '',
        expiration_date: '',
        days_left: '', //se tiene que calcular a partir de la fecha de expiración
        notification: false, //depende de days_left. 
        llamadas: null, 
        internet: null, 
        sms: null, 
        empty: false
      };
    }, 

    initialize: function(attributes, options) {
      if(options){
        this.msisdn = options.msisdn; 
      }
      this.llamadas = new TipoConsumo(); this.llamadas.set({name: "llamadas"});  
      this.internet = new TipoConsumo(); this.internet.set({name: "internet"});  
      this.sms = new TipoConsumo();  this.sms.set({name: "sms"});  
      this.fetchComboFromStorage(); 
      if(options && options.api){
        this.getData(); 
      }


      var listener = function() { 
        //this.attributes.days_left = this.attributes.total - this.attributes.consumidas; 
        //TODO calcular los días restantes. 
      };
      this.bind("change", listener);

    }, 

    fetchComboFromStorage: function(){
      var _this = this; 
      storage.loadCombo(this.msisdn, function(combo){
        if(combo!=null){

          if(combo.empty){
            _this.set({empty: true}); 
          } else{ 
            var llamadas = new TipoConsumo();
            llamadas.set({
              name: combo.llamadas.name,
              total: combo.llamadas.total, 
              consumidas:combo.llamadas.consumidas
            });
            var internet = new TipoConsumo();
            internet.set({
              name: combo.internet.name,
              total: combo.internet.total, 
              consumidas:combo.internet.consumidas
            });
            var sms = new TipoConsumo();
            sms.set({
              name: combo.sms.name,
              total: combo.sms.total, 
              consumidas:combo.sms.consumidas
            });


            _this.set({
              name: text.getComboName(combo.name),
              expiration_date: combo.expiration_date,
              days_left: combo.days_left, 
              notification: combo.notification, 
              llamadas: llamadas,
              internet: internet,
              sms: sms,
              empty: combo.empty
            });
          }
        }
      });

    }, 

    getData: function(){ //get Data from API

      var _this = this; 
      global.api.comboDetail(this.msisdn, function(error, response){
        if(error){
          console.log('API Error en combo para la línea ['+_this.msisdn+']= ' + error);
          _this.set({empty: true});
          _this.trigger('combo:complete', _this);
        }else{

          if(!response.response[0]){
            console.log('No hay combo para la línea: ' + _this.msisdn);
            _this.set({empty: true});
            _this.trigger('combo:complete', _this);
          } else {

            var llamadas = new TipoConsumo();
            llamadas.set({
              name: "llamadas",
              total: response.response[0].call_info.initial, 
              consumidas: response.response[0].call_info.current
            });
            var internet = new TipoConsumo();
            internet.set({
              name: "internet",
              total: response.response[0].data_info.initial, 
              consumidas: response.response[0].data_info.current
            });
            var sms = new TipoConsumo();
            sms.set({
              name: "sms",
              total: response.response[0].sms_info.initial, 
              consumidas: response.response[0].sms_info.current
            });

            _this.set({
                name : text.getComboName(response.response[0].product_name), 
                expiration_date : text.formatDate(response.response[0].expiration_date), 
                days_left : text.getDaysFromToday(text.formatDate(response.response[0].expiration_date)), //TODO se tiene que calcular a partir de la fecha de expiración
                notification : response.response[0].expiration_flag, 
                llamadas: llamadas, 
                internet: internet, 
                sms: sms 
            }); 
            
          }

          _this.saveInStorage();
          

        }

      }); 
    }, 

    saveInStorage: function() {
      var _this = this;
      storage.storeCombo(_this, function () {
        console.log('Combo guardado en localstorage: ' + _this.get('msisdn')); 
        _this.trigger('combo:complete', _this);       
      });
    }

  });

  return ComboDetail;
});
