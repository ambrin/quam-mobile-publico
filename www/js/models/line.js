define([
  'backbone',
  'utils/global', 
  'storage/auth', 
  'models/comboDetail'
], function (Backbone, global, storage, ComboDetail) {
  'use strict';

  var LineModel = Backbone.Model.extend({
    id: '', /* phone number */

    defaults: function () {
      return {
        balance: '',
        expiration_date: '',
        combo: new ComboDetail()
      };
    }, 

    initialize: function(){

      this.bind("change", function() { 
        
        console.log('cambio en la linea *** ');
        if(this.get('combo')!=null){
          console.log('combo changed');
          this.listenTo(this.get('combo'), 'combo:complete', this.comboCompleted);
        }        
      });
    }, 

    comboCompleted: function(){
      console.log('aaaaaaaaaaaaaaaaaaaaaaa');
    }

  });

  return LineModel;
});
