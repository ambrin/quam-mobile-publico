define([
  'backbone',
  'utils/global', 
  'storage/auth'
], function (Backbone, global, storage) {
  'use strict';

/*
  Punto de entrada de la app. Conprueba si ya ha hecho login alguna vez. 
  Si es así, redirige a la Home o se queda en la pantalla que estaba. Sino, redirige a login. 

  Además, va guardando qué peticiones ha hecho a la API. Cada 5 minutos lo resetea. Así, 
  no siempre que accedamos a una pantalla estará consultando, sino que mostrará los datos guardados anteriormente. 
  Todos estos datos se almacenan en el LocalStorage

*/
  var Auth = Backbone.Model.extend({
    defaults: function () {
      return {
        email: null,
        password: null,
        loggedIn: null,
        authToken: null, 
        checked: false, 
        type: '' //["normal", "facebook", "twitter", "google+"]
      };
    },

    initialize: function () {
      console.log('init de Models/auth. Activando listeners...'); 
      this.listenTo(this, 'change:authToken', this.storeCredentials);
      this.listenTo(this, 'change:checked', this.checkingCompleted);
    },

    checkCredentials: function () {
      console.log('checking credentials...');
      var _this = this;

      if(this.get('loggedIn')){ 
        global.router.navigate('home', {trigger: true})

      } else {
        //buscar en localstorage si tenemos datos. Si no es así, tiene que hacer login. 
        //si tenemos datos, mantenemos al usuario en la página en la que esté, salvo
        //si acaba de entrar, que le enviamos a la home
        storage.load(function (email, password, authToken, type) {

          if(!email || ! password){
            console.log('Not logged. Redirecting to login page...');
            global.router.navigate('login', { trigger: true });
          }else{
            global.api.setCredentials(email, password);
            global.api.setToken(authToken); 
            /*
              Si estamos en el inicio (index.html), y está logado, redirigimos a la home. 
              el resto de pantallas se quedan donde está, porque está logado.
            */
            if(Backbone.history.fragment == '' || Backbone.history.fragment == 'login'){
              console.log('User logged. Redirecting to HOME page...');
              $.mobile.changePage( "#home-page" , { transition: "slide",reverse: false} ); 
              global.router.navigate('home', { trigger: true });
            }            
            _this.set({checked: true});
          }
        });
      }
    },

    checkingCompleted: function(){
      if(this.get('checked')){   
        this.resetFunctionsStack();      
        this.trigger('credentials:complete', null);        
      }
      //arrancar timer
    }, 

    logout: function () {
      console.log('Logging out...');
      // The following will implicitely launch a redirect to login
      this.set({
        email: null,
        password: null,
        loggedIn: false,
        authToken: null, 
        type: ''
      });
    }, 

    storeCredentials: function(){
      
      storage.store(this.get('email'), this.get('password'), this.get('authToken'),this.get('type'), function () {
        console.log('localstorage actualizado');
        global.api.setCredentials(this.get('email'), this.get('password'));
        global.api.setToken(this.get('authToken'));        
      });
      this.set({checked: true});
    }, 

    resetFunctionsStack: function(){
      console.log('clearing stack of functions...');
      this.stack = new Array(); 
      var _this = this; 
      setTimeout(function(){
        _this.resetFunctionsStack(); 
        console.log('timer ejecutado!!');
        },5*10*3600); //cada 5 minutoa borramos
    }, 

    addFunctionToStack: function(functionName){
      this.stack.push(functionName); 
    }, 

    checkFunction:function(functionName){

      return ($.inArray(functionName, this.stack) !=-1); 
    }

  });

  return Auth;
});
