define([
  'backbone',
  'utils/global', 
  'models/acumulado',  
  'storage/auth', 
  'utils/text'
], function (Backbone, global, ConsumoAcumuladoPeriodo, storage, text) {

  'use strict';
  var ConsumptionsSummaryModel = Backbone.Model.extend({
    defaults: function () {
      return {
        today: null, 
        lastweek:null,
        lastmonth: null
      }; 
      
    }, 

    initialize: function(attributes, options) {
      if(options){
        this.msisdn = options.msisdn; 
      }
      this.fetchFromStorage();     
    }, 

    fetchFromStorage: function(){
      var _this = this; 
      storage.loadAcumulados(this.msisdn, function(acumulados){
        var consumptiontoday = new ConsumoAcumuladoPeriodo();
        var consumptionlastweek = new ConsumoAcumuladoPeriodo();
        var consumptionlastmonth = new ConsumoAcumuladoPeriodo();

        if(acumulados!=null){
         
          consumptiontoday.set({
            periodo: "today",
            llamadasentrantes: acumulados.today.llamadasentrantes, 
            llamadassalientes: acumulados.today.llamadassalientes,
            internet:acumulados.today.internet,
            sms: acumulados.today.sms
          });
          
          consumptionlastweek.set({
           periodo: "lastweek",
            llamadasentrantes: acumulados.lastweek.llamadasentrantes, 
            llamadassalientes: acumulados.lastweek.llamadassalientes,
            internet: acumulados.lastweek.internet,
            sms: acumulados.lastweek.sms
          });
          
          consumptionlastmonth.set({
            periodo: "lastmonth",
            llamadasentrantes: acumulados.lastmonth.llamadasentrantes, 
            llamadassalientes: acumulados.lastmonth.llamadassalientes,
            internet: acumulados.lastmonth.internet,
            sms: acumulados.lastmonth.sms
          });

        }

        _this.set({
             today : consumptiontoday,
             lastweek: consumptionlastweek,
             lastmonth: consumptionlastmonth
          });

      });

    }, 

    getData: function(){
       if(global.auth.get('checked')) { this.initConsumptionsSummary() }
       else {
          //esperamos a que haga la comprobación
          this.listenTo(global.auth, 'credentials:complete', this.initConsumptionsSummary);
        }
    }, 
    
    initConsumptionsSummary: function(){ //get Data from API

      var _this = this; 
      global.api.consumptionssumary(this.msisdn, function(error, response){
        if(error){
          console.log('API consumptionssumary: Error = ' + error);
        }else{
          var consumptiontoday = new ConsumoAcumuladoPeriodo();
          consumptiontoday.set({
            periodo: "today",
            llamadasentrantes: response.response.today.incoming_calls_duration, 
            llamadassalientes: response.response.today.outgoing_calls_duration,
            internet: response.response.today.internet_data,
            sms: response.response.today.messages_sent
          });
          var consumptionlastweek = new ConsumoAcumuladoPeriodo();
          consumptionlastweek.set({
           periodo: "lastweek",
            llamadasentrantes: response.response.last_week.incoming_calls_duration, 
            llamadassalientes: response.response.last_week.outgoing_calls_duration,
            internet: response.response.last_week.internet_data,
            sms: response.response.last_week.messages_sent
          });
          var consumptionlastmonth = new ConsumoAcumuladoPeriodo();
          consumptionlastmonth.set({
            periodo: "lastmonth",
            llamadasentrantes: response.response.last_month.incoming_calls_duration, 
            llamadassalientes: response.response.last_month.outgoing_calls_duration,
            internet: response.response.last_month.internet_data,
            sms: response.response.last_month.messages_sent
          });

          _this.set({
              today : consumptiontoday,
              lastweek: consumptionlastweek,
              lastmonth: consumptionlastmonth

          }); 
          
          console.log("elemento consumption lastmonth: "+JSON.stringify(consumptionlastmonth));
          console.log("elemento que se va a guardar:" +JSON.stringify(_this));
          _this.saveInStorage();
        }

      }); 
    }, 

    saveInStorage: function() {
      var _this = this;
      storage.storeAcumulados(_this, function () {
        console.log('Acumulados guardados en localstorage');
        _this.trigger('consumptionssummary:complete', _this);
      });
    }

  });


  return ConsumptionsSummaryModel;
});
