define([
  'backbone',
], function (Backbone) {
  'use strict';

  var TipoConsumo = Backbone.Model.extend({
    defaults: function () {
      return {
        name: null,
        total: 0,
        consumidas: 0, 
        restantes: 0 /* las restantes se tienen que calcular solas*/
      };
    }, 
    initialize: function() {
      var listener = function() { 
        this.attributes.restantes = this.attributes.total - this.attributes.consumidas; 
      };
      this.bind("change", listener);
    }

  });

  return TipoConsumo;
});
