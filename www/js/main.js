 // Sets the require.js configuration for your application.
require.config({

  // 3rd party script alias names (Easier to type "jquery" than "libs/jquery-1.8.3.min")
  paths: {
    jquery: 'vendor/jquery-1.10.2',
    jquerymobile: 'vendor/jquery.mobile-1.3.2',
    jqueryui: 'vendor/jquery-ui',
    underscore: 'vendor/underscore',
    backbone: 'vendor/backbone', 
    text: 'vendor/text', 
    cordova: 'vendor/cordova', 
    oauth: 'vendor/twitter/jsOAuth-1.3.7.js'
  },
  // Sets the configuration for your third party scripts that are not AMD compatible
  shim: {
    jquery: {
      exports: '$'
    },
    underscore: {
      exports: '_'
    },
    jquerymobile: {
       deps: ['jquery']
    },
    backbone: {
      deps: [ "underscore", "jquery" ],
      exports: "Backbone"  
    }, 
    jqueryui: {
      deps: ['jquery']
    },
    cordova: {
      deps: ['jquery']
    },
  }
});

require([
  'backbone',
  'jquery',
  'router', 
  'utils/global', 
  'models/auth', 
  'storage/auth'
], function (Backbone, $, Router, global, Auth, storage) {
  'use strict';


      _.templateSettings = {
        evaluate: /\[\[([\s\S]+?)\]\]/g,
        interpolate: /\{\{(.+?)\}\}/g // Moustache.js style
      };

      $(document).ready(
          // Set up the "mobileinit" handler before requiring jQuery Mobile's module
          function() {
            // Prevents all anchor click handling including the addition of active button state and alternate link bluring.
            $.mobile.linkBindingEnabled = false;
            // Disabling this will prevent jQuery Mobile from handling hash changes
            $.mobile.hashListeningEnabled = false;
            console.log('jquerymobile hashListeningEnabled set to false');
          }
      )

      require( ["jqueryui" , "jquerymobile" ], function() {
          // Instantiates a new Backbone.js Mobile Router   
          console.log('iniciando app...');
          global.auth = new Auth();   
          global.router = new Router();

          //si es una página que necesita autenticación, comprobamos que esté logado
          if(Backbone.history.fragment != 'login' && Backbone.history.fragment != 'password'){
            global.auth.checkCredentials(); 
          }

          var number = 1 + Math.floor(Math.random() * 4);
          $('.main-wrapper').addClass('bg'+number);

          

      }); 
  

           
});
