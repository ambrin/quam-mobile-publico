// Boilerplate to make the same code work on CommonJS (Node.js) and AMD (Require.js)
//
// Source: https://github.com/umdjs/umd
//
(function (root, factory) {
  'use strict';
  if (typeof exports === 'object') {
    // CommonJS
    module.exports = factory(require('request'));
  } else if (typeof define === 'function' && define.amd) {
    // AMD
    define(['./simpleRequest'], factory);
  } 
}(this, function (request) {
  'use strict';
  // Actual module implementation
  return {
    init: function(customOptions) {
      var authToken = null;
      var pub = {
        getToken: function() {
          return authToken;
        },

        login: function(email, password, callback) {
          var response= {"token": "5sss230343134323331393634373438"}; 
          authToken = response.token;
          callback(null, response);
        },

        recovery: function(email, callback) {
          var response= {"error": "Se produce un error. No se como es el mock de ok"}; 
          callback(null, response);
        },

        /* Authenticated methods*/
        customerLines: function(callback){
          var response= {
            "result": true, 
            "response": 
                {   //"54640000000000": {"expiration_date": "2014-01-29T14:36:16", "balance": 0.0}, 
                    "543517708993": {"expiration_date": "2013-12-21T10:43:04", "balance": 59.0}, 
                   // "543517708995": {"expiration_date": "2013-12-11T12:43:21", "balance": 0.0}, 
                    "541122500002": {"expiration_date": "2014-02-05T22:26:46", "balance": 200.0}
                }
          }; 
          callback(null, response);
        }, 
        
        comboDetail: function(msisdn, callback){
          var response= {
            "result": true, 
            "response": [
              {
                "call_info": {"current": 0, "initial": 100}, 
                "expiration_flag": false, 
                "order_id": 12089, 
                "expiration_date": "2014-01-08T13:22:20", 
                "data_info": {"current": 172, "initial": 3072}, 
                "sms_info": {"current": 2230, "initial": 5000}, 
                "product_name": "COMBO MES"
              }
              ]
            }; 
             
          callback(null, response);
        }, 

        payments: function(page, callback){
          var response= {
            "result": true, 
            "response": {
                "count": 200, 
                "previous": null, 
                "results": [
                  {
                    "date": "2013-11-26T00:00:00", 
                    "subtype": "COMBO_MES", 
                    "amount": 65.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-11-24T00:00:00", 
                    "subtype": "COMBO_SEMANAL", 
                    "amount": 79.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-11-12T00:00:00", 
                    "subtype": "COMBO_MES", 
                    "amount": 73.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-10-27T00:00:00", 
                    "subtype": "voucher", 
                    "amount": 55.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-10-19T00:00:00", 
                    "subtype": "credit_card", 
                    "amount": 112.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-10-18T00:00:00", 
                    "subtype": "voucher", 
                    "amount": 121.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-10-01T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 96.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-09-15T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 32.0, 
                    "type": "purchase"
                  }
                ], 
              "next": 2
            }
          }; 
          callback(null, response);
        }, 

        payments_recharge: function(page, callback){
          var response= {
            "result": true, 
            "response": {
                "count": 100, 
                "previous": null, 
                "results": [
                  {
                    "date": "2013-11-26T00:00:00", 
                    "subtype": "voucher", 
                    "amount": -25.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-11-24T00:00:00", 
                    "subtype": "credit_card", 
                    "amount": 29.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-11-12T00:00:00", 
                    "subtype": "credit_card", 
                    "amount": 33.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-10-27T00:00:00", 
                    "subtype": "voucher", 
                    "amount": 35.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-10-19T00:00:00", 
                    "subtype": "credit_card", 
                    "amount": 42.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-10-18T00:00:00", 
                    "subtype": "voucher", 
                    "amount": 81.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-10-01T00:00:00", 
                    "subtype": "voucher", 
                    "amount": 96.0, 
                    "type": "recharge"
                  }, 
                  {
                    "date": "2013-09-15T00:00:00", 
                    "subtype": "credit_card", 
                    "amount": -32.0, 
                    "type": "recharge"
                  }
                ], 
              "next": null
            }
          }; 
          callback(null, response);
        }, 

        payments_purchase: function(page, callback){
          var response= {
            "result": true, 
            "response": {
                "count": 100, 
                "previous": null, 
                "results": [
                  {
                    "date": "2013-11-26T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 65.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-11-24T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 79.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-11-12T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 73.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-10-27T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 55.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-10-19T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 112.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-10-18T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 121.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-10-01T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 96.0, 
                    "type": "purchase"
                  }, 
                  {
                    "date": "2013-09-15T00:00:00", 
                    "subtype": "COMBO_MINI", 
                    "amount": 32.0, 
                    "type": "purchase"
                  }
                ], 
              "next": 2
            }
          }; 
          callback(null, response);
        }, 

        consumption: function(msisdn,  usage, rate, cost, startDate, endDate, callback){
          var response = {
            "result": true, 
            "response": 
               [
                {"date": "2013-12-30T13:09:10.867000-04:17", "destination_phone": "912 654 123", "duration": 120, "type_usage": "VOICE", "cost": 10.2, "usage": "incoming"},
                {"date": "2013-12-30T13:09:10.867000-04:17", "destination_phone": "912 654 123", "duration": 120, "type_usage": "VOICE", "cost": 10.2, "usage": "incoming"}, 
                {"date": "2013-12-29T13:09:10.867000-04:17", "destination_phone": "912 654 123", "duration": 120, "type_usage": "VOICE", "cost": 10.2, "usage": "incoming"}, 
                {"date": "2013-12-29T13:09:10.867000-04:17", "destination_phone": "912 654 123", "duration": 12, "type_usage": "VOICE", "cost": 10.2, "usage": "incoming"}, 
                {"date": "2013-12-25T13:09:10.867000-04:17", "destination_phone": "912 654 123", "duration": 12, "type_usage": "VOICE", "cost": 10.2, "usage": "incoming"}, 
                {"usage": "adjustment", "date": "2013-12-26T15:09:10.867000-04:17", "type_usage": "ADJUSTMENT", "cost": 10.2}, 
                {"usage": "sms_out", "date": "2013-12-26T15:09:10.867000-04:17", "type_usage": "MMS", "cost": 10.2, "destination_phone": "912 654 123"}, 
                {"usage": "internet_in", "date": "2013-12-26T15:09:10.867000-04:17", "type_usage": "DATA", "cost": 10.2, "size": 36253}, 
                {"usage": "internet_in", "date": "2013-12-26T15:09:10.867000-04:17", "type_usage": "DATA", "cost": 10.2, "size": 36253}, 
                {"usage": "sms_out", "date": "2013-12-26T15:09:10.867000-04:17", "type_usage": "MMS", "cost": 10.2, "destination_phone": "912 654 123"}, 
                {"usage": "sms_out", "date": "2013-12-26T15:09:10.867000-04:17", "type_usage": "MMS", "cost": 10.2, "destination_phone": "912 654 123"}, 
                {"usage": "sms_out", "date": "2013-12-26T15:09:10.867000-04:17", "type_usage": "MMS", "cost": 10.2, "destination_phone": "912 654 123"}, 
                {"usage": "internet_in", "date": "2013-12-26T15:09:10.867000-04:17", "type_usage": "DATA", "cost": 10.2, "size": 36253}, 
                {"date": "2013-12-27T11:09:10.867000-04:17", "destination_phone": " 912 654 123", "duration": 65, "type_usage": "VOICE", "cost": 10.2, "usage": "outgoing"}, 
                {"usage": "sms_out", "date": "2013-12-27T01:09:10.867000-04:17", "type_usage": "SMS", "cost": 10.2, "destination_phone": "912 654 123"}, 
                {"usage": "sms_out", "date": "2013-11-27T01:09:10.867000-04:17", "type_usage": "SMS", "cost": 10.2, "destination_phone": "912 654 123"}, 
                {"date": "2013-11-27T11:09:10.867000-04:17", "destination_phone": " 912 654 123", "duration": 65, "type_usage": "VOICE", "cost": 10.2, "usage": "outgoing"}, 
                {"date": "2013-10-27T11:09:10.867000-04:17", "destination_phone": " 912 654 123", "duration": 65, "type_usage": "VOICE", "cost": 10.2, "usage": "outgoing"}, 
                {"usage": "sms_out", "date": "2013-10-27T01:09:10.867000-04:17", "type_usage": "SMS", "cost": 10.2, "destination_phone": "912 654 123"}
              ]
          }        
          callback(null, response);
        },

        consumptionssumary: function(msisdn, callback){
          var response = {
            "result": true, 
            "response":               
              { "today"     :  {"incoming_calls_duration": 123, "outgoing_calls_duration": 20, "internet_data": 650,"messages_sent": 65},
                "last_week" :  {"incoming_calls_duration": 787, "outgoing_calls_duration": 68, "internet_data": 1520,"messages_sent": 165},
                "last_month":  {"incoming_calls_duration": 1787, "outgoing_calls_duration": 168, "internet_data": 3520,"messages_sent": 365}
              }              
          }
          callback(null, response);
        }
      };
      return pub;
    }
  };
}));
