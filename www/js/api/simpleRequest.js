(function (root, factory) {
  'use strict';
  if (typeof exports === 'object') {
    // CommonJS
    module.exports = factory();
  } else if (typeof define === 'function' && define.amd) {
    // AMD
    define([], factory);
  } else {
    // Browser globals (root is window)
    root.simpleRequest = factory();
  }
}(this, function () {
  'use strict';
  return function(options, callback) {

    var request = new XMLHttpRequest({
      mozSystem: true
    });
    request.open(options.method, options.url, true);
    if (options.headers) {
      for (var i in options.headers) {
        request.setRequestHeader(i, options.headers[i]);
      }
    }

    var data = null;
    if (options.json) {
      request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
      data = JSON.stringify(options.json);
    } else if (options.data) {
      data = options.data;
    }

    request.onload = function() {
      var body;
      var response = {
        statusCode: this.status,
        response: this.responseText
      };
      try {
        body = this.responseText ? JSON.parse(this.responseText) : null;
      } catch(e) {
        body = this.responseText;
      }

      callback(null, response, body);
    };
    request.onerror = function(err) {
      callback(err, null, null);
    };
    request.send(data);
  };
}));
