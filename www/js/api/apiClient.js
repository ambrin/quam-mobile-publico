define([
  'storage/auth',
], function (storage) {
  'use strict';

  // Actual module implementation
  return {
    init: function(customOptions) {

      //estas variables son para las reconexiones cuando el token de simfonics caduca
      var email = null;
      var password = null;
      var authToken = null;
      var loginType = null; 
      var sessionExpiredErrors = 0; 

      var options = {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }, 
        baseUrl: 'http://localhost:8000/api'       
      };

      customOptions = customOptions || {};

      for (var i in options) {
        if (typeof customOptions[i] !== 'undefined') {
          options[i] = customOptions[i];
        }
      }
      
        
      var req = function(method, url, data, auth, callback) {      
        data = data || null;
        var headers = {};
        for (var i in options.headers) {
          headers[i] = options.headers[i];
        }
        if (authToken && auth) {
          headers.Authorization = 'Token ' + authToken;
        }

        $.ajax({
            url: options.baseUrl + url,
            type: method,
            beforeSend: function (request){              
                for (var i in headers) {
                  request.setRequestHeader(i, headers[i]);
                }
            },
            data: JSON.stringify(data),
            dataType: "json",
            success: function(response) {
                response.statusCode = this.statusCode; 
                if(response.error){

                  if(response.error ==='SessionExpired' && sessionExpiredErrors<5){
                    sessionExpiredErrors++; 
                    var error = {statusCode: '401', id: response.error, message: response.detail};
                  } else {
                    sessionExpiredErrors = 0; 
                    var error = {statusCode: '500', id: response.error, message: response.detail};                    
                  }
                  callback(error, response);

                }else{
                  sessionExpiredErrors = 0; 
                  callback(null, response);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
               console.log("error en API...");
               console.log(xhr);
               try{
                 if(xhr.responseJSON.detail==='Invalid token' && sessionExpiredErrors <5){
                    sessionExpiredErrors ++; 
                    var error = {statusCode: '401', id: '401', message: xhr.responseJSON.detail};
                    callback(error, null);
                 } else {
                    sessionExpiredErrors = 0; 
                    callback(xhr, null);
                 }
               }catch(e){
                  callback(xhr, null);
               }
            }
         });

      };

      var reqGet = function(url,data, callback) {
        req('GET', url, data, null, callback);
      };
      var reqGetAuth = function(url, data, callback) {
        req('GET', url, data, true, callback);
      };
      //peticion POST sin token de autenticacion
      var reqPost = function(url, data, callback) {
        req('POST', url, data, null, callback);
      };
      //peticion POST con token de autenticación
      var reqPostAuth = function(url, data, callback) {
        req('POST', url, data, true, callback);
      };

      var showLoading = function(){
        $('#loading').addClass('show');
      };
      var hideLoading = function(){
        $('#loading').removeClass('show');
      }; 

      /* TODO buscar una mejor forma de controlar la sesión expirada.. */
      var pub = {
        
        getToken: function() {
          return authToken;
        },
        setToken: function(token) {
          authToken=token;
        },
        setCredentials: function(mail, pwd) {
          email=mail;
          password=pwd;
        },
        setLoginType: function(type) {
          loginType=type;
        },

        login: function(email, password, callback) {
          console.log('API.login - parameters: [' + email + ',' + password + ']')
          showLoading(); 
          reqPost('/api-token/',{email: email, password: password}, function(error, response) {
            if (!error) {
              authToken = response.token;
            }
            callback(error, response);
            hideLoading(); 
          });
        },

        loginFacebook: function(userId, fbToken, callback){
          console.log('API.loginFacebook - parameters: [' + userId + ',' + fbToken + ']')
          showLoading(); 

          reqPost('/api-token/',{nt: '1', token: fbToken, id: userId}, function(error, response) {
            if (!error) {
              authToken = response.token;
              console.log('token devuelto por simfonics: '+ authToken);
            }
            callback(error, response);
            hideLoading(); 
          });
        }, 

        loginTwitter: function(oauthToken, oauthTokenSecret, callback){
          console.log('API.loginTwitter - parameters: [' + oauthToken + ', '+ oauthTokenSecret + ']'); 
          showLoading(); 

          var finalToken = 'oauth_token_secret=' + oauthTokenSecret +'&amp;oauth_token=' + oauthToken;
          reqPost('/api-token/',{nt: '2', token: finalToken}, function(error, response) {
            if (!error) {
              authToken = response.token;
              console.log('token devuelto por simfonics: '+ authToken);
            }
            callback(error, response);
            hideLoading(); 
          });

        },

        loginGoogle: function(userId, googleToken, callback){
          console.log('API.loginGoogle - parameters: [' + userId + ',' + googleToken + ']')
          showLoading(); 

          reqPost('/api-token/',{nt: '3', token: googleToken, id: userId}, function(error, response) {
            if (!error) {
              authToken = response.token;
              console.log('token devuelto por simfonics: '+ authToken);
            }
            callback(error, response);
            hideLoading(); 
          });
        }, 

        retryLogin: function(callback){
          console.log('SessionExpired. Relogin...'); 
          
          storage.load(function (e, p, authToken, type) {
            email = e; 
            password = p; 

            if(type=='normal'){
              this.login(email, password, callback);
            }else if (type=='facebook'){
              this.loginFacebook(email, password, callback); //en email se guarda el userId de facebook, y en password el 
              //token de facebook.
            }else if (type=='twitter'){
              this.loginTwitter(email, password, callback); //en email se guarda el oauth_token y en password el 
              //oauth_token_secret de twitter.
            }else if (type=='google'){
              this.loginGoogle(email, password, callback); //en email se guarda el googleId de facebook, y en password el 
              //googleToken de google.
            }
          });    
          
          
        }, 

        recovery: function(email, callback) {
          console.log('API.recovery - parameters: [' + email + ']');
          showLoading(); 
          reqPost('/forgot-password/', {email: email},  function(error, response){
            hideLoading(); 
            callback(error, response); 
          });
        }, 

        /* Authenticated methods*/
        //Home: líneas contratadas
        customerLines: function(callback){
          console.log('API.customerLines'); 
          var _this = this; 
          reqGetAuth('/customer-account/', null, function(error, response){
            //mirar si es porque la sesion ha caducado
            if(error && error.statusCode==401){
              _this.retryLogin(function(){
                _this.customerLines(callback);
              });
            }else{
              callback(error, response);
            } 
          });
        },

        //Home: combos por cada línea
        comboDetail: function(msisdn, callback){
          console.log('API.comboDetail - parameters: ['+msisdn+']');
          $('#loading-combo').addClass('show');
          $('li#combo-detail').css('opacity', '0.6'); 
          var _this = this; 
          reqGetAuth('/customer-account/'+msisdn, null, function(error, response){
            $('#loading-combo').removeClass('show');
            $('li#combo-detail').css('opacity', '0.8'); 
            
            //mirar si es porque la sesion ha caducado
            if(error && error.statusCode==401){
              _this.retryLogin(function(){
                _this.comboDetail(msisdn, callback);
              });
            }else{
              callback(error, response);
            }  

          });
        }, 

        //Movimientos de saldo: todos
        payments: function(page, callback){
          console.log('API.payments - parameters: ['+page+']');
          showLoading(); 
          var _this = this; 
          var url = '/payments/'; 
          if(page){
            url = url + '?page=' + page; 
          }
          console.log(url);
          reqGetAuth(url, null, function(error, response){
            hideLoading(); 
            //mirar si es porque la sesion ha caducado
            if(error && error.statusCode==401){
              _this.retryLogin(function(){
                _this.payments(page, callback);
              });
            }else{
              callback(error, response);
            }  
          });
         
        }, 

        //Movimientos de saldo: solo recargas
        payments_recharge: function(page, callback){
          console.log('API.payments_recharge - parameters: ['+page+']');
          showLoading();
          var _this = this; 
          var url = '/payments/recharge'; 
          if(page){
            url += '?page=' + page; 
          }
          reqGetAuth(url, null, function(error, response){
            hideLoading();
            //mirar si es porque la sesion ha caducado
            if(error && error.statusCode==401){
              _this.retryLogin(function(){
                _this.payments_recharge(page, callback);
              });
            }else{
              callback(error, response);
            }  
          });
        }, 

        //Movimientos de saldo: soolo compras
        payments_purchase: function(page,callback){
          console.log('API.payments_purchase - parameters: ['+page+']');
          showLoading(); 
          var _this = this; 
          var url = '/payments/purchase'; 
          if(page){
            url += '?page=' + page; 
          }
          reqGetAuth(url, null, function(error, response){
            hideLoading(); 
            //mirar si es porque la sesion ha caducado
            if(error && error.statusCode==401){
              _this.retryLogin(function(){
                _this.payments_purchase(page, callback);
              });
            }else{
              callback(error, response);
            }             
          });
        }, 

        consumption: function(msisdn,  usage, rate, cost, startDate, endDate, callback){
          console.log('API.consumption - parameters: ['+msisdn+ ',' +usage +',' + rate + ',' +cost + ',' +startDate + ',' +endDate +']');
          showLoading(); 
          var _this = this; 
          var data = {"usage": usage, "rate": rate, "cost": cost, "end_date": endDate, "start_date": startDate} 
          reqPostAuth('/consumption/'+msisdn, data, function(error, response){
            hideLoading(); 
            //mirar si es porque la sesion ha caducado
            if(error && error.statusCode==401){
              _this.retryLogin(function(){
                _this.consumption(msisdn, usage, rate, cost, startDate, endDate, callback);
              });
            }else{
              callback(error, response);
            }           
          });
        }, 

        //Consumos: resumen
        consumptionssumary: function(msisdn, callback){
          console.log('API.consumptionssumary - parameters: ['+msisdn+']');
          showLoading(); 
          var _this = this; 
          reqGetAuth('/consumption/summary/'+msisdn, null, function(error, response){
            hideLoading(); 

            //mirar si es porque la sesion ha caducado
            if(error && error.statusCode==401){
              _this.retryLogin(function(){
                _this.consumptionssumary(msisdn, callback);
              });
            }else{
              callback(error, response);
            }
                         
          });
        }
       
      };
      return pub;
    }
  };

});
