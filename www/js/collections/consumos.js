define([
  'backbone',
  'utils/global', 
  'models/consumption',
  'storage/auth',
  'utils/text'
], function (Backbone, global, Consumption, storage, text) {
  'use strict';

  var ConsumosCollection = Backbone.Collection.extend({
    model: Consumption,
    msisdn: null, 

    usages: function(){
      return ['all', 'VOICE', 'incoming', 'outgoing', 'SMS', 'DATA'];
    }, 
    rates: function(){
      return ['all', 'standard', 'international', 'roaming'];
    }, 
    costs: function(){
      return ['all', 'sincoste', 'costecombo', 'costesaldo'];
    },

    initialize: function (attributes, options){ 
         
      this.msisdn = options.msisdn;
      this.endDate = text.getToday(); 
      this.startDate = text.monthAgo();      
      this.fetchConsumosFromStorage();
        
    }, 
    /*
       Busca las lineas del usuario en el LocalStorage, para que primero visualice estas, 
       y luego por detrás hace la petición a la API. 
    */
    fetchConsumosFromStorage: function(){
      console.log('coleccion consumos, fechConsumosFromStorage');
      var _this = this; 
      storage.loadConsumptions(function(consumption){
        if(consumption){
          _this.reset();
          $.each(consumption, function(i, item) { 
            //_this.add(_this.parseConsumption(item), {merge: true});
            _this.add(item, {merge:true});
          });
          _this.trigger('consumptions:complete', _this.models);        
          console.log('consumos recuperados de LocalStorage: ' + _this.models.length);
        }        
      });
    },

    getData: function(){
      if(global.auth.get('checked')) { this.initConsumption() }
      else {
        //esperamos a que haga la comprobación
        this.listenTo(global.auth, 'credentials:complete', this.initConsumption);
      }  
    }, 

    initConsumption: function(startDate, endDate, usage, rate, cost){
      console.log('coleccion consumos, getData');
      //Si no nos viene informado startDate ni endDate, hay que coger el último mes: 
      //   - endDate = hoy
      //   - startDate = hoy - 1mes
      //nota: si mandamos un null en esos parámetros, hace el mismo comportamiento

      if(!startDate) startDate = this.startDate; 
      if(!endDate) endDate = this.endDate;
      if(!usage) usage = this.usages()[0]; 
      if(!rate) rate = this.rates()[0]; 
      if(!cost) cost = this.costs()[0]; 

      //Lamada a la API
      var _this = this; 
      global.api.consumption(this.msisdn, usage, rate, cost, startDate, endDate, function(error, response){
        if(error){
          console.log('Error while attempting to get consumptions...');
        }else{
          _this.reset();  
          $.each(response.response, function(index, item) {  
              //console.log("dentro");           
            _this.add(_this.parseConsumption(item), {merge: true});
          });
          _this.trigger('consumptions:complete', _this.models);          
          _this.saveInStorage();//TODO Solo guardamos los consumos del último mes. 
        }
      
      }); 
    },

    parseConsumption: function(item){
      console.log('coleccion consumos, parseConsumption');
      //console.log('lo que recibidmos de la api: '+item);
      var c= new Consumption({
        /*type: item.type,
        usage: item.usage, 
        date: text.formatDateHours(item.date),
        name: 'No se', 
        value: item.cost   
        */
        
        date: text.formatDateHours(item.date),
        destination_phone: item.destination_phone,
        duration: item.duration,
        size: item.size,
        cost: item.cost,
        usage: item.usage


      });

      return c; 
    }, 

    filterByLastWeek: function(){
      this.fetchConsumosFromStorage(); 
      /*Para la última semana siempre recuperarmos todo, por si venimos de los datos de hoy*/
      console.log('coleccion consumos, filterByLastWeek');
      var start = text.weekAgo();
      this.reset(this.byDate(start, this.endDate), {msisdn: this.msisdn});  
    }, 
    
    filterByToday: function(){
      console.log('coleccion consumos, filterByToday');
      var today=text.getToday();
      this.reset(this.byDateToday(), {msisdn: this.msisdn}); 
    },

    filterByMonth: function(){
      console.log('coleccion consumos, filterByMonth');
      this.fetchConsumosFromStorage();    
    },

    byDate: function(startDate, endDate) {
      console.log('coleccion consumos, byDate');
      var _this = this; 
      var filtered = _this.filter(function(consumption) {
        return text.isBetween(consumption.get("date"),startDate, _this.endDate);
        });  
      return filtered;
    },
    byDateToday: function() {
      console.log('coleccion consumos, byDateToday');
      var _this = this; 
      var filtered = _this.filter(function(consumption) {
        console.log('Datos filtrados:'+ text.isToday(consumption.get("date"),_this.endDate));
        return text.isToday(consumption.get("date"),_this.endDate);
        });
      return filtered;
    },

    /* Envía petición al LocalStorage para que guarde las líneas 
       El trigger mejor lo hacemos antes para que no tenga que estar esperando
       al callback del LocalStorage
    */
    saveInStorage: function(){
      
      storage.storeConsumptions(this, function () {
      console.log('"Consumos" successfully saved in LocalStorage');
      });
    }, 

    /* TODO hacer los filtros usando los filtros de Backbone collections*/

  });

  return ConsumosCollection;
});
