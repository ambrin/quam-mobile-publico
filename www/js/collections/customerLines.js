define([
  'backbone',
  'utils/global', 
  'models/line', 
  'models/comboDetail',
  'storage/auth', 
  'utils/text'
], function (Backbone, global, Line, ComboDetail, storage, text) {
  'use strict';

  var CustomerLines = Backbone.Collection.extend({
    model: Line,
    selectedLine : null,
    functionName: 'customerLines', 
    
    initialize: function (){    
      this.fetchLinesFromStorage();      
    }, 

    getData: function(){  
      if(global.auth.get('checked') ){
        this.initCustomerLines(); 
      }
      else {
        //esperamos a que haga la comprobación
        this.listenTo(global.auth, 'credentials:complete', this.initCustomerLines);
      }      
    }, 

    initCustomerLines: function(){
      if(global.auth.checkFunction(this.functionName)){
        console.log('Info existente. No llamamos a la API');
        return; 
      }
      //Lamada a la API
      var _this = this; 
      global.api.customerLines(function(error, response){
        if(error){
          console.error('Error while attempting to get customerLines');
        }else{
          _this.reset(); 
          $.each(response.response, function(msisdn, item) {             
             var line= new Line({
                id: msisdn,
                balance: item.balance,
                expiration_date: text.formatDate(item.expiration_date), 
                combo: new ComboDetail({}, {msisdn: msisdn, api: false})                
              });
            _this.add(line, {merge: true});
            _this.listenTo(line.get('combo'), 'combo:complete', _this.comboCompleted);
          });
          _this.selectedLine = _this.models[0]; 
          _this.trigger('lines:complete', _this.models); //que vaya pintando las líneas
          _this.saveInStorage();
          _this.getComboInfo(); 
          global.auth.addFunctionToStack(_this.functionName);
        }
      
      }); 
    },

    /* recorre todas las lineas y busca para cada una de ellas la info de su combo*/
    getComboInfo: function(){
      var _this = this; 
      $.each(_this.models, function(index, item) {
        item.get('combo').getData(); 
      });
    },

    /*
       Busca las lineas del usuario en el LocalStorage, para que primero visualice estas, 
       y luego por detrás hace la petición a la API. 
    */
    fetchLinesFromStorage: function(){
      var _this = this; 
      storage.loadCustomerLines(function(lines){
        if(lines){
          $.each(lines, function(i, item) {
             var line= new Line({
                id: item.id,
                balance: item.balance,
                expiration_date: text.formatDate(item.expiration_date),
                combo: new ComboDetail({}, {msisdn: item.id, api: false})
              });
              _this.add(line, {merge: true});
          });
          _this.selectedLine = _this.models[0]; 
          console.log('lineas recuperadas de LocalStorage: ' + _this.models.length);
        }
        
      }); 

      
    }, 

    /* Envía petición al LocalStorage para que guarde las líneas 
       El trigger mejor lo hacemos antes para que no tenga que estar esperando
       al callback del LocalStorage
    */
    saveInStorage: function(){
      storage.storeCustomerLines(this, function () {
        console.log('CustomerLines successfully saved in LocalStorage');
      });
    },

    getSelectedLine: function (){
      return this.selectedLine ? this.selectedLine : new Line();
    }, 

    changeLine: function (msisdn){     
      if (this.selectedLine.id!=msisdn){
        this.selectedLine = this.get(msisdn);
        this.trigger('lines:reset', this.models);
      }
    }, 
    comboCompleted: function(combo){
      console.log("combo ya tiene sus datos. Informar a la home");
      console.log(combo); 
      /* se informa a la home para que pinte el combo, solo si es la linea seleccionada */
      if(combo.msisdn == this.getSelectedLine().id){
        this.trigger('combo:populate', combo); 
      }
    }

  });

  return CustomerLines;
});
