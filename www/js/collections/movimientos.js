define([
  'backbone',
  'utils/global', 
  'models/movimiento',
  'storage/auth',
  'utils/text'
], function (Backbone, global, Payment, storage, text) {
  'use strict';

  var MovimientosSaldo = Backbone.Collection.extend({
    model: Payment,
    count: 0, /* total de movimientos del usuario */
    currentPage: 0, /* Tiene paginación */
    rechargesList: null, 
    purchasesList: null,
    currentPageRecharges: 1, 
    currentPagePurchases: 1,
    
    initialize: function (){    
      this.fetchMovimientosFromStorage();      
    }, 

    /*
       Busca las lineas del usuario en el LocalStorage, para que primero visualice estas, 
       y luego por detrás hace la petición a la API. 
    */
    fetchMovimientosFromStorage: function(){
      var _this = this; 
      storage.loadPayments(function(payment){
        if(payment){
          $.each(payment, function(i, item) { 
            _this.add(_this.parsePayment(item), {merge: true});
          });          
          console.log('movimientos de saldo recuperados de LocalStorage: ' + _this.models.length);
        }        
      }); 
    },

    getData: function(){
      if(global.auth.get('checked')) { this.initPayments() }
      else {
        //esperamos a que haga la comprobación
        this.listenTo(global.auth, 'credentials:complete', this.initPayments); 
      }
    }, 
    
    initPayments: function(){
   
      //Lamada a la API
      var _this = this; 
      global.api.payments(this.currentPage, function(error, response){
        if(error){
          console.log('Error while attempting to get payments...');
        }else{
          if(_this.currentPage==1){
            _this.reset(); //primera página del listado
          }
          _this.count = response.response.count; 
          _this.currentPage = (response.response.next) ? response.response.next -1 : response.response.previous +1; 
          $.each(response.response.results, function(index, item) {             
            _this.add(_this.parsePayment(item), {merge: true});
          });
          _this.trigger('payments:complete', _this.models);
          if(response.response.next!=null){
            _this.trigger('show:more', _this.models);
          } 
          if(_this.currentPage==1){
            _this.saveInStorage(); //solo se guarda el primer listado
          }
          _this.currentPage++; 
        }
      
      }); 
    },

    parsePayment: function(item){

      var payment= new Payment({
        type: item.type,
        subtype: item.subtype, 
        date: text.formatDate(item.date),
        name: this.getPaymentName(item.type, item.subtype), 
        value: item.amount     
      });
      return payment; 
    }, 

    getPaymentName: function(type, subtype){
      if(type==="purchase"){
        return text.capitalize(subtype);
      } else if(type==="recharge" && subtype==="voucher"){
        return "Recarga con vale"; 
      } else if(type==="recharge" && subtype==="credit_card"){
        return "Recarga con tarjeta"; 
      }

      return "-";
    },

    /*    FILTRADO POR RECARGAS 
    Punto de entrada: cuando se selecciona la pestaña "Recargas"
    1º Si ya tenemos datos, pintamos esos
    2º Si no hay datos, llamamos a la API, y los guardamos para cuando los volvamos a necesitar
    */
    filterRechargedPayments: function(init){
      var _this = this; 

      if(this.rechargesList && this.rechargesList.length>0){
        console.log('ya hay recargas.... pintando: ' + this.rechargesList.length);
        _this.trigger('recharges:reset');
        $.each(_this.rechargesList, function(index, item) { 
          _this.trigger('recharges:paint', item);
        });
        _this.trigger('recharges:complete', _this.rechargesList.length);
      } else {
        // TODO obtener los del localStorage
        _this.getRechargesFromApi();      
      }
    },

    // Llama a la API para pedir el listado de recargas. Va con paginación 
    getRechargesFromApi: function(){
      var _this = this; 

      global.api.payments_recharge(_this.currentPageRecharges, function(error, response){
          if(error){
            console.log('Error while attempting to get payments');
          }else{
            if(_this.currentPageRecharges==1){
              //significa que es la 1ª consulta, y podríamos tener datos antiguos recuperados del Localstorage
              _this.rechargesList = new Array();
              _this.trigger('recharges:reset');
            } 

            $.each(response.response.results, function(index, item) { 
              var p =  _this.parsePayment(item);            
              _this.rechargesList.push(p);
              _this.trigger('recharges:paint', p);//va pintando uno a uno
            });
            _this.trigger('recharges:complete', _this.rechargesList.length);
            //cuando termine todo, que la vista muestre un boton para seguir buscando
            if(response.response.next!=null){
              _this.trigger('show:more');
            } 
            if(_this.currentPageRecharges==1){              
              _this.storeRecharges(); //solo guardamos en LocalStorage el primer listado
            }            
            _this.currentPageRecharges++; 
            console.log('Recargas pintadas desde API. Total: ' + _this.rechargesList.length); 
          }
        
        }); 
    },

    fetchRechargesFromStorage: function(){
      var _this = this; 
      _this.rechargesList = new Array(); 
      storage.loadRecharges(function(payment){
          if(payment){
            $.each(payment, function(i, item) { 
              var p =  _this.parsePayment(item);            
              _this.rechargesList.push(p);
              _this.trigger('recharges:paint', p);//va pintando uno a uno
            });       
            _this.trigger('recharges:complete', _this.rechargesList.length);
          }        
        });
    }, 


    /*      FILTRADO POR COMPRAS 
    1º Si ya tenemos datos, pintamos esos
    2º Si no hay datos, llamamos a la API, y los guardamos para cuando los volvamos a necesitar
    */
    filterPurchasedPayments: function(page){
      var _this = this; 

      if(this.purchasesList && this.purchasesList.length>0){
        console.log('ya hay compras.... pintando: ' + this.purchasesList.length);
        _this.trigger('purchases:reset');
        $.each(this.purchasesList, function(index, item) { 
          _this.trigger('purchases:paint', item);
        });
        _this.trigger('purchases:complete', _this.purchasesList.length);
      } else {
        //TODO Obtener los que hay en el LocalStorage, y mientras se va accediendo a la API
        this.fetchPurchasesFromStorage(); 
        this.getPurchasesFromApi();         
      }
    },

    // Llama a la API para pedir el listado de compras. Va con paginación 
    getPurchasesFromApi: function(){
      var _this = this; 

      global.api.payments_purchase(this.currentPagePurchases, function(error, response){
        if(error){
          console.log('Error while attempting to get payments');
        }else{
          if(_this.currentPagePurchases==1){
            //significa que es la 1ª consulta, y podríamos tener datos antiguos recuperados del Localstorage
            _this.purchasesList = new Array(); 
            _this.trigger('purchases:reset');
          }
          $.each(response.response.results, function(index, item) {       
            var p =  _this.parsePayment(item);            
            _this.purchasesList.push(p);
            _this.trigger('purchases:paint', p);  //va pintando uno a uno
          });
          _this.trigger('purchases:complete', _this.purchasesList.length);
          if(response.response.next!=null){
            _this.trigger('show:more');
          } 
          if(_this.currentPagePurchases==1){            
            _this.storePurchases(); //solo guardamos en LocalStorage el primer listado
          }
          _this.currentPagePurchases++;
          console.log('Compras pintadas desde API. Total: ' + _this.purchasesList.length); 
        }
      
      });
    }, 
   
    fetchPurchasesFromStorage: function(){
      var _this = this; 
      _this.purchasesList = new Array(); 
      storage.loadPurchases(function(payment){
          if(payment){
            $.each(payment, function(i, item) { 
              var p =  _this.parsePayment(item);            
              _this.purchasesList.push(p);
              _this.trigger('purchases:paint', p);//va pintando uno a uno
            }); 
            _this.trigger('purchases:complete', _this.purchasesList.length);                 
          }        
        });
    }, 

    /* Envía petición al LocalStorage para que guarde las líneas 
       El trigger mejor lo hacemos antes para que no tenga que estar esperando
       al callback del LocalStorage
    */
    saveInStorage: function(){
      storage.storePayments(this, function () {
        console.log('"Movimientos de saldo" successfully saved in LocalStorage');
      });
    }, 

    storeRecharges: function(){
      storage.storeRecharges(this.rechargesList);
    }, 

    storePurchases: function(){
      storage.storePurchases(this.purchasesList); 
    },

    getRechargesList: function(){
      return this.rechargesList; 
    }, 

    getPurchasesList: function(){
      return this.purchasesList;
    }

  });

  return MovimientosSaldo;
});
